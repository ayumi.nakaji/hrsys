from django.apps import AppConfig


class EmployeeListConfig(AppConfig):
    name = 'employee_list'
